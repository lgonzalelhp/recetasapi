<?php
 /*
Fichero de utilidades
 */
 
include_once 'libJWT/BeforeValidException.php';
include_once 'libJWT/ExpiredException.php';
include_once 'libJWT/SignatureInvalidException.php';
include_once 'libJWT/JWT.php';
include_once "libPHPMailer/class.phpmailer.php";
include_once "libPHPMailer/class.smtp.php";
use \Firebase\JWT\JWT;


/*
Función para enviar emails de confirmación
Recibe el email del destinatario y el token de acceso para construir la url
*/

function sendConfirmEmail($useremail, $jwt){

    $email_user = "pc1322pc1322@gmail.com";
    $email_password = "pass123456#";
    $the_subject = "Activar cuenta recetasDAW";
    $from_name = "RecetasDAW";
    $phpmailer = new PHPMailer();

    $phpmailer->Username = $email_user;
    $phpmailer->Password = $email_password; 
 
    $phpmailer->SMTPDebug = 1;
    $phpmailer->SMTPSecure = 'tls';
    $phpmailer->Host = "smtp.gmail.com"; 
    $phpmailer->Port = 587;
    $phpmailer->IsSMTP(); 
    $phpmailer->SMTPAuth = true;

    $phpmailer->setFrom($phpmailer->Username,$from_name);
    $phpmailer->AddAddress($useremail); 

    $phpmailer->Subject = $the_subject;	
    $phpmailer->Body .="<p>Para activar tu cuenta accede al siguiente enlace<br>";
    $phpmailer->Body .= "<a href='http://localhost/recetas/activate.php?token=".$jwt."'>Activar cuenta</a></p>";
    $phpmailer->IsHTML(true);

    $phpmailer->Send();
}
/*
Función para enviar emails de actulizacion de contraseña
Recibe el email del destinatario, la nueva contraseña, y el nombre de usuario
*/
function sendNewPassEmail($useremail, $password, $username){

    $email_user = "pc1322pc1322@gmail.com";
    $email_password = "pass123456#";
    $the_subject = "Activar cuenta recetasDAW";
    $from_name = "RecetasDAW";
    $phpmailer = new PHPMailer();

    $phpmailer->Username = $email_user;
    $phpmailer->Password = $email_password; 

    $phpmailer->SMTPDebug = 1;
    $phpmailer->SMTPSecure = 'tls';
    $phpmailer->Host = "smtp.gmail.com"; 
    $phpmailer->Port = 587;
    $phpmailer->IsSMTP(); 
    $phpmailer->SMTPAuth = true;

    $phpmailer->setFrom($phpmailer->Username,$from_name);
    $phpmailer->AddAddress($useremail); 

    $phpmailer->Subject = $the_subject;	
    $phpmailer->Body .="<p> Para acceder a tu cuenta utiliza los siguientes datos<br>";
    $phpmailer->Body .="Usuario: ".$username."<br>";
    $phpmailer->Body .="Contraseña ".$password."</p>";
    $phpmailer->IsHTML(true);

    $phpmailer->Send();

 
}
/*
Función para establecer la conexión con la base de datos
*/
function connect($db)
{
    try {
        $conn = new PDO("mysql:host={$db['host']};dbname={$db['db']};charset=utf8", $db['username'], $db['password']);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    } catch (PDOException $exception) {
        exit($exception->getMessage());
    }
}

//Asociar todos los parametros a un sql
function bindAllValues($statement, $params)
{
foreach($params as $param => $value)
{
    $statement->bindValue(':'.$param, $value);
}
return $statement;
}
/*
Función para establecer las cabeceras de las respuestas http
*/
function setHeaders(){
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Allow-Methods: *');
    header('content-type: application/json; charset=utf-8');
    
}
/*
Función para determinar si un token es valido
Recibe un token
Devuelve si este es valido o no mediante un boolean
*/
function validateJWT($jwt){
    if($jwt){
        try {
            $key="RecetasDAW";
            $decoded = JWT::decode($jwt, $key, array('HS256'));
            return true;
        }
        catch (Exception $e){
            return false;
        }
    }else{

        return false;
    }
}
/*
Función para extraer la informacion asociada a un JWT
Recibe un token
Devuelve la información asociada
*/
function extractJWTData($jwt){
    if($jwt){
        try {
            $key="RecetasDAW";
            $decoded = JWT::decode($jwt, $key, array('HS256'));
            $res["id"]=$decoded->data->id;
            $res["username"]=$decoded->data->username;
            return $res;
        }
        catch (Exception $e){
            return false;
        }
    }else{

        return false;
    }
}
/*
Función para generar un token JWT
Recibe un datos que se asocian al token (username)
Devuelve el token
*/
function generateJWT($result){
    $current = date_timestamp_get(date_create());
    $key="RecetasDAW";
    $token = array(
        "iat" => $current,
        "nbf" => $current,
        "exp" => $current + (60*60),
        "data" => array(
        "id" => $result['id'],
        "username" => $result['username']
        )
    );
    return JWT::encode($token, $key);
}

?>