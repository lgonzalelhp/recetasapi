<?php
/*
  Fichero con la funcionalidad gestionar los ingredientes en función del metodo  
  por el que se realiza la petición. Para poder ser atendida la petición debe ser enviada con
  un token de acceso válido.
  */
  include "config.php";
  include "utils.php";
  $dbConn =  connect($db);
  
  setHeaders();
  $data = json_decode(file_get_contents("php://input"), true);
  
  if (isset($_GET['jwt'])){
    $jwt=$_GET['jwt'];
  }else if($data["jwt"]!=""){
    $jwt=$data['jwt'];
  }else{
    $jwt="";
  }

  if (validateJWT($jwt)){
    $datareg=extractJWTData($jwt);
    //Obtener los ingredientes asociados a un id de categoria
    if ($_SERVER['REQUEST_METHOD'] == 'GET')
    {  
      $sql = "SELECT * FROM ingredientes where id_categoria='".$_GET['id_categoria']."'";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(array ("jwt" => generateJWT($datareg), "ings" => $statement->fetchAll(PDO::FETCH_ASSOC) ) );
      exit();
    }
    // Crear un nuevo ingrediente asociado a una categoria dada
    if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
      $input = file_get_contents("php://input");
      $input=json_decode($input, true);
      $sql = "INSERT INTO ingredientes
            (nombre, descripcion, precio, unidad, id_usuario, id_categoria)
            VALUES
            ('".$input["nombre"]."', '".$input["descripcion"]."', '".$input["precio"]."', '".$input["unidad"]."', '".$input["id_usuario"]."','".$input["id_categoria"]."')";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      $ingredienteId = $dbConn->lastInsertId();
      if($ingredienteId)
      {
        header("HTTP/1.1 200 OK");
        echo json_encode(array ("jwt" => generateJWT($datareg), "id" => $ingredienteId ) );
        exit();
      }
    }
    //Borrar un ingrediente por id de ingrediente
    if ($_SERVER['REQUEST_METHOD'] == 'DELETE')
    {
      $input = file_get_contents('php://input');
      $input=json_decode($input, true);
      $sql = "DELETE FROM ingredientes where id='".$input["id"]."'";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(array ("jwt" => generateJWT($datareg), "id" => $input["id"] ) );
      //echo json_encode($input["id"]);
      exit();
    }
    //Actualizar un ingrediente, se peuden actualizar los datos del ingredeitne por id de ingredietne
    //Sino se puede cambiar la asociación del ingrediente a una categoría
    if ($_SERVER['REQUEST_METHOD'] == 'PUT')
    {
      $input = file_get_contents("php://input");
      $input=json_decode($input, true);
      if ( $input["action"] == "edit" ){
        $sql = "UPDATE ingredientes SET nombre='".$input["nombre"]."', descripcion='".$input["descripcion"]."', precio='".$input["precio"]."', unidad='".$input["unidad"]."' WHERE id='".$input["id"]."'";
      }else if ( $input["action"] == "move" ){
        $sql = "UPDATE ingredientes SET id_categoria='".$input["categoria"]."' WHERE id='".$input["id"]."'";
      }
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      header("HTTP/1.1 200");
      echo json_encode(array ("jwt" => generateJWT($datareg) ) );
    }
  }
?>