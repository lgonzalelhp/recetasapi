<?php
/*
  Fichero con la funcionalidad gestionar los ingredientes asociados a las recetas en función del metodo  
  por el que se realiza la petición. Para poder ser atendida la petición debe ser enviada con
  un token de acceso válido.
  */
  include "config.php";
  include "utils.php";
  $dbConn =  connect($db);
  
  setHeaders();
  $data = json_decode(file_get_contents("php://input"), true);
  
  if (isset($_GET['jwt'])){
    $jwt=$_GET['jwt'];
  }else if($data["jwt"]!=""){
    $jwt=$data['jwt'];
  }else{
    $jwt="";
  }

  if (validateJWT($jwt)){
    $datareg=extractJWTData($jwt);
    //Obtener los ingredientes y los datos de los ingredientes asociados a un id de receta
    if ($_SERVER['REQUEST_METHOD'] == 'GET')
    {  
      $sql = "SELECT ing_esta_receta.*, ingredientes.nombre, ingredientes.precio, ingredientes.unidad FROM ing_esta_receta JOIN ingredientes on ing_esta_receta.id_ingrediente=ingredientes.id WHERE ing_esta_receta.id_receta='".$_GET['id_receta']."'";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(array ("jwt" => generateJWT($datareg), "ings" => $statement->fetchAll(PDO::FETCH_ASSOC) ) );
      exit();
    }
    // Asociar un ingrediente a una receta
    if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
      $input = file_get_contents("php://input");
      $input=json_decode($input, true);
      $sql = "INSERT INTO ing_esta_receta
            (id_receta, id_ingrediente, cantidad, merma)
            VALUES
            ('".$input["id_receta"]."', '".$input["id_ingrediente"]."', '".$input["cantidad"]."', '".$input["merma"]."')";
      $statement = $dbConn->prepare($sql);
      try{
        $statement->execute();
        header("HTTP/1.1 200 OK");
        echo json_encode(array ("jwt" => generateJWT($datareg)) );
      }catch(Exception $e){
        $sql = "SELECT nombre FROM ingredientes  WHERE id='".$input["id_ingrediente"]."'";
        $statement = $dbConn->prepare($sql);
        $statement->execute();
        $res=$statement->fetch(PDO::FETCH_ASSOC);
        header("HTTP/1.1 200 OK");
        echo json_encode(array ("jwt" => generateJWT($datareg), "msg" => $res) );
      }
      
      
      exit();
     
    }
    //Borrar un ingrediente asociado a un receta
    if ($_SERVER['REQUEST_METHOD'] == 'DELETE')
    {
      $input = file_get_contents('php://input');
      $input=json_decode($input, true);
      $sql = "DELETE FROM ing_esta_receta where id_receta='".$input["id_receta"]."'";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(array ("jwt" => generateJWT($datareg)) );
      //echo json_encode($input["id"]);
      exit();
    }
  }
?>