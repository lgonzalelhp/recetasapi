<?php
 /*
  Fichero con la funcionalidad para activar las cuentas de usuario, solo atiende peticiones mediante mediatne GET
  */
include "config.php";
include "utils.php";
$dbConn =  connect($db);
  
  $data = json_decode(file_get_contents("php://input"), true);
  
  if (isset($_GET['token'])){
    $jwt=$_GET['token'];
  }else{
    $jwt="";
  }

  if (validateJWT($jwt)){
    $datareg=extractJWTData($jwt);
    if ($_SERVER['REQUEST_METHOD'] == 'GET')
    {  
      $sql = "UPDATE usuarios SET activada=true WHERE id='".$datareg["id"]."'";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      header("HTTP/1.1 200 OK");
      echo "Cuenta activada correctamente. Redirigiendo automaticamente";
    }
  }else{
    echo "Error al activar la cuenta. Redirigiendo automaticamente";
  }
  sleep(5);
  header("Location: http://localhost:8080");
  die();
?>