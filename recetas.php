<?php
/*
  Fichero con la funcionalidad gestionar las recetas en función del metodo  
  por el que se realiza la petición. Para poder ser atendida la petición debe ser enviada con
  un token de acceso válido.
  */
  include "config.php";
  include "utils.php";
  $dbConn =  connect($db);
  
  setHeaders();
  $data = json_decode(file_get_contents("php://input"), true);
  
  if (isset($_GET['jwt'])){
    $jwt=$_GET['jwt'];
  }else if($data["jwt"]!=""){
    $jwt=$data['jwt'];
  }else{
    $jwt="";
  }

  if (validateJWT($jwt)){
    $datareg=extractJWTData($jwt);
    //Obtener las recetas asociadas a un grupo segun su id de grupo
    if ($_SERVER['REQUEST_METHOD'] == 'GET')
    {  
      $sql = "SELECT * FROM receta where id_grupo='".$_GET['id_grupo']."'";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(array ("jwt" => generateJWT($datareg), "recetas" => $statement->fetchAll(PDO::FETCH_ASSOC) ) );
      exit();
    }
    // Crear una nueva recetas asociandola a un id de grupo
    if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
      $input = file_get_contents("php://input");
      $input=json_decode($input, true);
      $sql = "INSERT INTO receta
            (nombre, observaciones, raciones, tiempo, id_usuario, id_grupo)
            VALUES
            ('".$input["nombre"]."', '".$input["observaciones"]."', '".$input["raciones"]."', '".$input["tiempo"]."', '".$input["id_usuario"]."','".$input["id_grupo"]."')";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      $recetaID = $dbConn->lastInsertId();
      if($recetaID)
      {
        header("HTTP/1.1 200 OK");
        echo json_encode(array ("jwt" => generateJWT($datareg), "id" => $recetaID ) );
        exit();
      }
    }
    //Borrar una receta a partir del id de receta
    if ($_SERVER['REQUEST_METHOD'] == 'DELETE')
    {
      $input = file_get_contents('php://input');
      $input=json_decode($input, true);
      $sql = "DELETE FROM receta where id='".$input["id"]."'";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      unlink("Fotos/".$input["id"].".png");
      header("HTTP/1.1 200 OK");
      echo json_encode(array ("jwt" => generateJWT($datareg), "id" => $input["id"] ) );
      //echo json_encode($input["id"]);
      exit();
    }
    //Actualizar una receta, se peuden actualizar los datos de la receta por id de receta
    //Sino se puede cambiar la asociación de la receta a un grupo
    if ($_SERVER['REQUEST_METHOD'] == 'PUT')
    {
      $input = file_get_contents("php://input");
      $input=json_decode($input, true);
      if ( $input["action"] == "edit" ){
        $sql = "UPDATE receta SET nombre='".$input["nombre"]."', observaciones='".$input["observaciones"]."', raciones='".$input["raciones"]."', tiempo='".$input["tiempo"]."' WHERE id='".$input["id"]."'";
      }else if ( $input["action"] == "move" ){
        $sql = "UPDATE receta SET id_grupo='".$input["grupo"]."' WHERE id='".$input["id"]."'";
      }
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      header("HTTP/1.1 200");
      echo json_encode(array ("jwt" => generateJWT($datareg) ) );
    }
  }
?>