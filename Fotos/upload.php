<?php
  include "../utils.php";
  /*
  Fichero con la funcionalidad para renombrar y alamencar las fotos asociadas a las recetas,
  solo se atienden peticiones mediante post
  */
  setHeaders();
    if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $dir_subida = '';
        $fichero_subido = $dir_subida . basename($_FILES['fileToUpload']['name']);
        
        if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $fichero_subido)) {
            rename($fichero_subido, $_POST["id"].".png");
            header("HTTP/1.1 200");
            echo json_encode(array ("status" => "OK" ) );
        } else {
            header("HTTP/1.1 400");
            echo json_encode(array ("status" => "FAILS" ) );
        }
      
    }
  ?>