<?php
/*
  Fichero con la funcionalidad gestionar las categorias de ingredientes en función del metodo  
  por el que se realiza la petición. Para poder ser atendida la petición debe ser enviada con
  un token de acceso válido.
  */
  include "config.php";
  include "utils.php";
  $dbConn =  connect($db);
  
  setHeaders();
  $data = json_decode(file_get_contents("php://input"), true);
  
  if (isset($_GET['jwt'])){
    $jwt=$_GET['jwt'];
  }else if($data["jwt"]!=""){
    $jwt=$data['jwt'];
  }else{
    $jwt="";
  }

  if (validateJWT($jwt)){
    $datareg=extractJWTData($jwt);
    //Obtener las categorias por id de usuario
    if ($_SERVER['REQUEST_METHOD'] == 'GET')
    {  
      $sql = "SELECT * FROM categoria_ingredientes where id_usuario='".$_GET['id_usuario']."'";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(array ("jwt" => generateJWT($datareg), "cats" => $statement->fetchAll(PDO::FETCH_ASSOC) ) );
      exit();
    }
    // Crear una nueva categoria 
    if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
      $input = file_get_contents("php://input");
      $input=json_decode($input, true);
      $sql = "INSERT INTO categoria_ingredientes
            (id_usuario, nombre, descripcion)
            VALUES
            ('".$input["id_usuario"]."', '".$input["nombre"]."', '".$input["descripcion"]."')";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      $categoriaId = $dbConn->lastInsertId();
      if($categoriaId)
      {
        header("HTTP/1.1 200 OK");
        echo json_encode(array ("jwt" => generateJWT($datareg), "id" => $categoriaId ) );
        exit();
      }
    }
    //Borrar una categoria por id
    if ($_SERVER['REQUEST_METHOD'] == 'DELETE')
    {
      $input = file_get_contents('php://input');
      $input=json_decode($input, true);
      $sql = "DELETE FROM categoria_ingredientes where id='".$input["id"]."'";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(array ("jwt" => generateJWT($datareg), "id" => $input["id"] ) );
      //echo json_encode($input["id"]);
      exit();
    }
    //Actualizar una categoria por id
    if ($_SERVER['REQUEST_METHOD'] == 'PUT')
    {
      $input = file_get_contents("php://input");
      $input=json_decode($input, true);
      $sql = "UPDATE categoria_ingredientes SET nombre='".$input["nombre"]."', descripcion='".$input["descripcion"]."' WHERE id='".$input["id"]."'";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      header("HTTP/1.1 200");
      echo json_encode(array ("jwt" => generateJWT($datareg) ) );
    }
  }
?>