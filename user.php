<?php
/*
  Fichero con la funcionalidad gestionar usuarios en función del metodo  
  por el que se realiza la petición. Para poder ser atendida la petición debe ser enviada con
  un token de acceso válido.
  */
  include "config.php";
  include "utils.php";

  $dbConn =  connect($db);
  
  setHeaders();
    //Crear un nuevo usuario en la base de datos
    if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $input = file_get_contents("php://input");
        $input=json_decode($input, true);
        $password_hash = password_hash($input["password"], PASSWORD_BCRYPT);
        $sql = "INSERT INTO usuarios
            (username, email, password)
            VALUES
            ('".$input["username"]."', '".$input["email"]."', '".$password_hash."')";
        $statement = $dbConn->prepare($sql);
        try {
            $statement->execute();
            $userID = $dbConn->lastInsertId();
            if($userID)
            {
            header("HTTP/1.1 200 OK");
            $datareg["id"]=$userID;
            $datareg["username"]=$input["username"];
            $jwt=generateJWT($datareg);
            sendConfirmEmail($input["email"], $jwt);
            echo json_encode(array ("msg" => "Creado"  ) );
            exit();
            }
        }catch (Exception $e){
            header("HTTP/1.1 400 OK");
            echo json_encode(array ("msg" => "No se puede crear, el usuario y/o email ya esta registrado" ) );
        }
       
       
    }
    //Actualizar un usuario, se peuden actualizar los campos de un id de usuario en funcion de la acción
    if ($_SERVER['REQUEST_METHOD'] == 'PUT')
    {
      $input = file_get_contents("php://input");
      $input=json_decode($input, true);
      $datareg=extractJWTData($input["jwt"]);
      if (validateJWT($input["jwt"])){
        if ( $input["action"] == "edit_username" ){
            $sql = "UPDATE usuarios SET username='".$input["username"]."' WHERE id='".$input["id"]."'";
        }else if ( $input["action"] == "edit_email" ){
            $sql = "UPDATE usuarios SET email='".$input["email"]."' WHERE id='".$input["id"]."'";
        }else if ( $input["action"] == "edit_password" ){
            $password_hash = password_hash($input["password"], PASSWORD_BCRYPT);
            $sql = "UPDATE usuarios SET password='".$password_hash."' WHERE id='".$input["id"]."'";
        }
        $statement = $dbConn->prepare($sql);
        $statement->execute();
        header("HTTP/1.1 200");
        echo json_encode(array ("jwt" => generateJWT($datareg) ) );
      }      
    }
    //Borrar un usuario y los datos asociados a el a partir del id de usuario
    if ($_SERVER['REQUEST_METHOD'] == 'DELETE')
    {
        $input = file_get_contents("php://input");
        $input=json_decode($input, true);
        $datareg=extractJWTData($input["jwt"]);
        if (validateJWT($input["jwt"])){
            $sql = "DELETE FROM usuarios where id='".$input["id"]."'";
            $statement = $dbConn->prepare($sql);
            $statement->execute();
            header("HTTP/1.1 200 OK");
            echo json_encode(array ("jwt" => generateJWT($datareg), "id" => $input["id"] ) );
            //echo json_encode($input["id"]);
            exit();
        }
    }