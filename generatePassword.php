<?php
/*
Fichero que atiende las peticiones mediante el metodo put de actualizacion de contraseñas
se genera una nueva contraseña que es enviada por correo electrónico
*/
include "config.php";
  include "utils.php";

  $dbConn =  connect($db);
  
  setHeaders();

    if ($_SERVER['REQUEST_METHOD'] == 'PUT')
    {   
        try{
            $input = file_get_contents("php://input");
            $input=json_decode($input, true);
            $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            $password = "";
            for($i=0;$i<8;$i++) {
                $password .= substr($str,rand(0,62),1);
            }
            
            $password_hash = password_hash($password, PASSWORD_BCRYPT);
            $sql = "UPDATE usuarios SET password='".$password_hash."' WHERE email='".$input["email"]."'";
            $statement = $dbConn->prepare($sql);
            $statement->execute();

            $sql = "SELECT username FROM usuarios where email='".$input["email"]."'";
            $statement = $dbConn->prepare($sql);
            $statement->execute();
            $result=$statement->fetch(PDO::FETCH_ASSOC);
            header("HTTP/1.1 200 OK");
            sendNewPassEmail($input["email"], $password, $result["username"]);
            echo json_encode(array ("msg" => "Actualizado"  ) );
        }catch (Exception $e){
            header("HTTP/1.1 400 OK");
            echo json_encode(array ("msg" => "No hay usuario con ese email registrado" ) );
        }
        exit();
    }

?>
