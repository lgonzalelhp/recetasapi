<?php
/*
  Fichero con la funcionalidad gestionar las instruciones asociadas a una receta en función del metodo  
  por el que se realiza la petición. Para poder ser atendida la petición debe ser enviada con
  un token de acceso válido.
  */
  include "config.php";
  include "utils.php";
  $dbConn =  connect($db);
  
  setHeaders();
  $data = json_decode(file_get_contents("php://input"), true);
  
  if (isset($_GET['jwt'])){
    $jwt=$_GET['jwt'];
  }else if($data["jwt"]!=""){
    $jwt=$data['jwt'];
  }else{
    $jwt="";
  }

  if (validateJWT($jwt)){
    $datareg=extractJWTData($jwt);
    //Obtener las instruciones asociadas a un id de receta
    if ($_SERVER['REQUEST_METHOD'] == 'GET')
    {  
      $sql = "SELECT * FROM instrucciones where id_receta='".$_GET['id_receta']."' ORDER BY posicion_instruccion";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(array ("jwt" => generateJWT($datareg), "insts" => $statement->fetchAll(PDO::FETCH_ASSOC) ) );
      exit();
    }
    // Asociar una nueva instrución a una receta segun el id de receta
    if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
      $input = file_get_contents("php://input");
      $input=json_decode($input, true);
      $sql = "INSERT INTO instrucciones
            (id_receta, texto_instruccion, posicion_instruccion)
            VALUES
            ('".$input["id_receta"]."', '".$input["texto_instruccion"]."', '".$input["posicion_instruccion"]."')";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(array ("jwt" => generateJWT($datareg)) );
      exit();
     
    }
    //Borrar una instrución asociada a un id de receta
    if ($_SERVER['REQUEST_METHOD'] == 'DELETE')
    {
      $input = file_get_contents('php://input');
      $input=json_decode($input, true);
      $sql = "DELETE FROM instrucciones where id_receta='".$input["id_receta"]."'";
      $statement = $dbConn->prepare($sql);
      $statement->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(array ("jwt" => generateJWT($datareg)) );
      //echo json_encode($input["id"]);
      exit();
    }
  }
?>