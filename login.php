<?php
/*
Fichero de  login de usuarios, solo atiende peticiones POST
*/
include "config.php";
include "utils.php";
$dbConn =  connect($db);


setHeaders();

if ($_SERVER['REQUEST_METHOD'] == "POST")
{
  $input = file_get_contents("php://input");
  $input=json_decode($input, true);
  $sql = "SELECT * FROM usuarios where activada=1 and username='".$input["username"]."'";
  $statement = $dbConn->prepare($sql);
  $statement->execute();
  $result = $statement->fetch(PDO::FETCH_ASSOC);
  if (!empty($result)){
    if (password_verify($input['password'],$result['password'])){
      header("HTTP/1.1 200");
      $response = array('username' => $result['username'],'email' => $result["email"], 'id' => $result['id'], "jwt" => generateJWT($result) );
      echo json_encode($response);
    }else{
      header("HTTP/1.1 401");
    }
  } else{
    header("HTTP/1.1 401");
  }
      
}else{
  die("No Other Methods Allowed");
}
  
exit();

?>
